import java.io.*;

import javax.xml.bind.DatatypeConverter;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class read_data {
	public static final int BLOCK_SIZE = 16;

	public static final String tweakString = "0123456789ABCDEF";

	public static void main(String[] args) throws IOException {
		
		// read file
		String folderLocation = "C:\\sagungrp\\Backup Adit\\Kuliah\\Semester 8\\CIS\\Tugas-1";
		
		Path wiki_path = Paths.get(folderLocation, "out2.PDF");
		byte[] file = null;
		try {
			file = Files.readAllBytes(wiki_path);
		} catch (IOException e) {
			System.out.println(e);
		}

		// convert file to array of bytes
		int fileByteCount = 0;
		int j = 0;
		boolean needStealing = true;
		int unusedSpace = 0;
		if(file.length % BLOCK_SIZE == 0) {
			j = file.length/BLOCK_SIZE;
			needStealing = false;
		}
		else {
			j = (file.length/BLOCK_SIZE)+1;
			needStealing = true;
			unusedSpace = BLOCK_SIZE - (file.length % BLOCK_SIZE);
		}

		// Group the message to 2d array
		// column (first dimension) is per block in file
		// row (second dimension) is per byte in one block
		// each block has BLOCK_SIZE bytes
		byte[][] fileBlocks = new byte[j][BLOCK_SIZE];
		for(int i = 0; i < j; i++) {
			for(int k = 0; k < BLOCK_SIZE; k++) {
				if(fileByteCount < file.length) {
					fileBlocks[i][k] = file[fileByteCount];
					fileByteCount++;
				}
			}
		}

		// Read key
		BufferedReader inputKey = new BufferedReader(new FileReader(new File(folderLocation + "\\key.txt")));
		// Key still in HEX
		String keyStr = inputKey.readLine();
		int keyLength = keyStr.length();
		String keyHex1 = keyStr.substring(0, keyLength/2);
		String keyHex2 = keyStr.substring(keyLength/2, keyLength);
		
		// Convert each key to its char/ascii
		int a = 0;
		String key1 = "";
		while(a < keyHex1.length()) {
			String temp = keyHex1.substring(a,a+2);
			int hex = Integer.parseInt(temp, BLOCK_SIZE);
			key1 += (char)hex;
			a = a+2;
		}
		byte[] key1arr = key1.getBytes();
		
		a = 0;
		String key2 = "";
		while(a < keyHex2.length()) {
			String temp = keyHex2.substring(a,a+2);
			int hex = Integer.parseInt(temp, BLOCK_SIZE);
			key2 += (char)hex;
			a = a+2;
		}
		byte[] key2arr = key2.getBytes();
		System.out.println("file length" + file.length + "\n" + "j:" + j + "\n" + "key1: " + key1 + "\n" + "key2: " +  key2);
		
		// Tweak
		byte[] tweakArr = tweakString.getBytes();
		byte[] tweak = new byte[tweakArr.length];

		// Make it little-endian
		for(int idx = 0; idx < tweakArr.length; idx++) {
			tweak[tweakArr.length-(idx+1)] = tweakArr[idx];
		}
		
		XTS xts = new XTS();

		// Encrypt
		byte[][] ciphertext = xts.xts(XTS.DECRYPT, fileBlocks, j, key1arr, key2arr, tweak, needStealing, unusedSpace);
		
		//printTwo2DByte(fileBlocks, ciphertext);
		System.out.println("========================================");
		
		// write ciphertext file
		writeToFile(ciphertext, folderLocation + "\\balik.PDF", file.length);
		
		// Close files
		inputKey.close();

		// write file
		// FileOutputStream fw = new FileOutputStream(folderLocation + "\\out.PNG");
		// fw.write(file);
		// fw.close();

	}
	
	public static void writeToFile(byte[][] target, String filename, int messageLength) throws IOException{	
		byte[] flattenedArray = new byte[messageLength];
		int globalID = 0;
		for(int xID = 0; xID < target.length; xID++){
			for(int yID = 0; yID <= 15; yID++){
				if(globalID < messageLength){
					flattenedArray[globalID] = (target[xID][yID]);
					globalID++;
				}
			}
		}
		
		// printing to file
		Path filepath = Paths.get(filename);
		Files.write(filepath, flattenedArray);
	}

	public static void printTwo2DByte(byte[][] target1, byte[][] target2){
		if(target1.length == target2.length){
			for(int p = 0; p < target1.length; p++) {
				for(int q = 0; q < target1[p].length; q++) {
					System.out.print(target1[p][q]);
					System.out.print(" - ");
					System.out.print(target2[p][q]);
					System.out.println();
				}
				System.out.println();
			}
		}
		else {
			System.out.println("Panjang array tidak sama");
		}
	}
	
}
