import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class XTS{
    public static final int ENCRYPT = 0;
    public static final int DECRYPT = 1;
    public static final int BLOCK_SIZE = 16;
    
    //Alpha
    //Irreducible of Galois field 128
    public static final int alpha = 135;

    

    public static byte[][] xts(int activity, byte[][] blockMessages, int j, byte[] key1, byte[] key2, byte[] tweak, boolean needStealing, int unusedSpace){
        
        //Create an AES for plaintext using key 1
        AES block1 = new AES();
        block1.setKey(key1);

        //Create an AES for tweak using key 2
        AES block2 = new AES();
        block2.setKey(key2);

        byte[][] PP = new byte[j][BLOCK_SIZE];
        byte[][] CC = new byte[j][BLOCK_SIZE];
        byte[][] ciphertext = new byte[j][BLOCK_SIZE];

        // Make T
        // key2 + i => encrypt by AES => tweak
        byte[] encryptedTweak = block2.encrypt(tweak);
        
        byte[][] multiplier = new byte[j + 1][BLOCK_SIZE];
        multiplier[0] = encryptedTweak;
        for(int i = 0; i < j; i++){
            for(int k = 0; k < BLOCK_SIZE; k++){
                if(k == 0){
                    multiplier[i+1][k] = (byte)((2 * (multiplier[i][k] % 128)) ^ (alpha * (multiplier[i][15] / 128)));
                } 
                else {
                    multiplier[i+1][k] = (byte)((2 * (multiplier[i][k] % 128)) ^ ((multiplier[i][k - 1] / 128)));
                }
            }
        }

        // block length has to be at least 3
        if(j < 3){
            System.out.println("File terlalu kecil");
        }
        else {

            // If the number of bytes is a multiple of BLOCK_SIZE
            if(!needStealing){
                // Calculate PP for every block except for the last and second-last
                for(int i = 0; i < j; i++){
                    for(int p = 0; p < BLOCK_SIZE; p++){
                        PP[i][p] = (byte) (blockMessages[i][p] ^ multiplier[i + 1][p]);
                    }
                }
                
                // Calculate CC for every block except for the last
                for(int i = 0; i < j; i++){
                    CC[i] = (activity == XTS.ENCRYPT) 
                        ? block1.encrypt(PP[i]) 
                        : block1.decrypt(PP[i]); 
                }

                // Calculate ciphertext for every block except for the last
                for(int i = 0; i < j; i++){
                    for(int p = 0; p < BLOCK_SIZE; p++){
                        ciphertext[i][p] = (byte) (CC[i][p] ^ multiplier[i + 1][p]);
                    }
                }
            }

            // If the number of bytes is not a multiple of BLOCK_SIZE
            else{
                // Calculate PP for every block except for the last and second-last
                for(int i = 0; i < j - 2; i++){
                    for(int p = 0; p < BLOCK_SIZE; p++){
                        PP[i][p] = (byte) (blockMessages[i][p] ^ multiplier[i + 1][p]);
                    }
                }
                
                // Calculate CC for every block except for the last
                for(int i = 0; i < j - 2; i++){
                    CC[i] = (activity == XTS.ENCRYPT) 
                        ? block1.encrypt(PP[i]) 
                        : block1.decrypt(PP[i]); 
                }

                // Calculate ciphertext for every block except for the last
                for(int i = 0; i < j - 2; i++){
                    for(int p = 0; p < BLOCK_SIZE; p++){
                        ciphertext[i][p] = (byte) (CC[i][p] ^ multiplier[i + 1][p]);
                    }
                }


                // STEALING for the last and second-last block
                
                // second-last block
                int pointer = j - 2;

                // shift once if encrpyt, twice if decrypt
                int shifter = (activity == XTS.ENCRYPT) ? 1 : 2;

                // PP
                for(int p = 0; p < BLOCK_SIZE; p++){
                    PP[pointer][p] = (byte) (blockMessages[pointer][p] ^ multiplier[pointer + shifter][p]);
                }

                // CC
                CC[pointer] = (activity == XTS.ENCRYPT) 
                    ? block1.encrypt(PP[pointer]) 
                    : block1.decrypt(PP[pointer]); 
                
                // ciphertext
                for(int p = 0; p < BLOCK_SIZE; p++){
                    ciphertext[pointer][p] = (byte) (CC[pointer][p] ^ multiplier[pointer + shifter][p]);
                }

                // Last block
                pointer = j - 1;
                
                // Fill in the last block with bytes from the second-last block
                int startPoint = BLOCK_SIZE - unusedSpace;
                int endPoint = BLOCK_SIZE;
                byte[] stolenBlock = new byte[BLOCK_SIZE];
                
                for(int i = 0; i < BLOCK_SIZE; i++){
                    stolenBlock[i] = blockMessages[j - 1][i];
                }

                for(int i = startPoint; i < endPoint; i++){
                    stolenBlock[i] = blockMessages[j - 1][i];
                }
                // PP 
                for(int p = 0; p < BLOCK_SIZE; p++){
                    PP[pointer][p] = (activity == XTS.ENCRYPT)
                        ? (byte) (stolenBlock[p] ^ multiplier[pointer + 1][p])
                        : (byte) (stolenBlock[p] ^ multiplier[pointer][p]);
                }

                // CC
                CC[pointer] = (activity == XTS.ENCRYPT)
                    ? block1.encrypt(PP[pointer])
                    : block1.decrypt(PP[pointer]);
                
                // ciphertext
                for(int p = 0; p < BLOCK_SIZE; p++){
                    ciphertext[pointer][p] = (activity == XTS.ENCRYPT) 
                        ? (byte) (CC[pointer][p] ^ multiplier[pointer + 1][p])
                        : (byte) (CC[pointer][p] ^ multiplier[pointer][p]);
                }

                // Swap the last and second-last block
                byte[] finalBlock = new byte[BLOCK_SIZE];
                for(int i = 0; i < BLOCK_SIZE; i++){
                    finalBlock[i] = ciphertext[j-1][i];
                }
                // copy cropped block j-2 to last block
                for(int i = 0; i < BLOCK_SIZE; i++){
                    
                    if(i < startPoint){
                        ciphertext[j - 1][i] = ciphertext[j - 2][i];
                    }
                    else {
                        ciphertext[j - 1][i] = (byte) 0;
                    }
                }
                // copy last block (original) to block j - 2 
                for(int i = 0; i < BLOCK_SIZE; i++){
                    ciphertext[j - 2][i] = finalBlock[i];
                }
            }
        }

        return ciphertext;
    }
}