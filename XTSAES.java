import java.io.*;

import javax.xml.bind.DatatypeConverter;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import java.math.BigInteger;
import java.nio.file.Files;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.util.Scanner;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.ButtonGroup;
import javax.swing.JButton;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.event.ActionEvent;
public class XTSAES {

	private JFrame frame;
	private JTextField txtPath;

	public static void main(String[] args) throws IOException {


			   Path wiki_path = Paths.get("C:\\Users\\USER\\workspace\\XTS-AES", "test_img.JPG");
			   FileOutputStream fw = new FileOutputStream("C:\\Users\\USER\\workspace\\XTS-AES\\test_img_res.JPG");
			   byte[] arr = null;
			   try {
			     arr = Files.readAllBytes(wiki_path);
			   } catch (IOException e) {
			     System.out.println(e);
			   }
			   fw.write(arr);
			   fw.close();
			   XTSAES window = new XTSAES();	
			   window.frame.setVisible(true);			
	}

	 //Change image to byte[]
	 public static byte[] filetoByteArray(String imagePath) throws IOException{
		File fi = new File(imagePath);
     	byte[] imageCipherByte = Files.readAllBytes(fi.toPath());
     	return imageCipherByte;
     	
	 }

	  public static String readKeyText(String keyPath) throws IOException{
		BufferedReader reader = null;	
		//read untuk KEY
        File file = new File(keyPath);
        reader = new BufferedReader(new FileReader(file));
        String lineKey = reader.readLine();
        reader.close();
        return lineKey;
	 }

	//Method GUI browse file
	public XTSAES() {
		initialize();
	}
		 
	 //implementasi GUI
		  private void initialize() {
		    frame = new JFrame();
		    frame.setBounds(100, 100, 600, 300);
		    // x,y, width,height
		    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		    frame.getContentPane().setLayout(null);
		    frame.getContentPane().setBackground(new Color(255,255,255));
		    
		    //Label GUI
		    JLabel label = new JLabel("XTS AES Encryption/Decryption");
		    label.setBounds(100, 10, 375, 24);
		    label.setFont(new Font("Roboto Bold", Font.PLAIN, 18));
		    frame.getContentPane().add(label);
		    
		 	// input key file
		    JLabel labelKey = new JLabel("Key :");
		    labelKey.setBounds(10, 90, 50, 24);
		    frame.getContentPane().add(labelKey);
		    
		    txtPath = new JTextField();
		    txtPath.setBounds(50, 90, 414, 24);
		    frame.getContentPane().add(txtPath);
		    txtPath.setColumns(10);
		         
		    JButton btnBrowse = new JButton("Browse");
		    btnBrowse.setBounds(475, 90, 87, 23);

		    btnBrowse.setBackground(new Color(0, 0, 204));
		    btnBrowse.setForeground(new Color(255, 255, 255));
		    frame.getContentPane().add(btnBrowse);
		    
		    
		 	// input File
		    JLabel labelText = new JLabel("Plain :");
		    labelText.setBounds(10, 50, 40, 24);
		    frame.getContentPane().add(labelText);
		    
		    JTextField txtPath2 = new JTextField();
		    txtPath2.setBounds(50, 50, 414, 24);
		    frame.getContentPane().add(txtPath2);
		    txtPath.setColumns(10);
		    
		    JButton btnBrowse1 = new JButton("Browse");
		    btnBrowse1.setBounds(475, 50, 87, 23);

		    btnBrowse1.setBackground(new Color(0, 0, 204));
		    btnBrowse1.setForeground(new Color(255, 255, 255));
		    frame.getContentPane().add(btnBrowse1);
		    
		    //Button encrypt dan decrypt
		    JButton btnEncrypt = new JButton("Encrypt");
		    btnEncrypt.setBounds(50, 130, 87, 23);
		    btnEncrypt.setBackground(new Color(118, 123, 145));
		    btnEncrypt.setForeground(new Color(255, 255, 255));
		    frame.getContentPane().add(btnEncrypt);
		    
		    JButton btnDecrypt = new JButton("Decrypt");
		    btnDecrypt.setBounds(150, 130, 87, 23);
		    btnDecrypt.setBackground(new Color(118, 123, 145));
		    btnDecrypt.setForeground(new Color(255, 255, 255));
		    frame.getContentPane().add(btnDecrypt);
		    
		    // Footer
		    JLabel footer = new JLabel("<html>Created with love by Sang Agung, Putri Ramadhanty & Andhita Nurul Ainun");
		    footer.setBounds(75, 220, 450, 40);
		    footer.setFont(new Font("Roboto Bold", Font.ITALIC, 13));
		    footer.setFont(new Font("Roboto Bold", Font.PLAIN, 14));
		    btnDecrypt.setForeground(new Color(29, 45, 68));
		    frame.getContentPane().add(footer);


		    
		    /**btnDecrypt.addActionListener(new ActionListener() {
		    	
			      public void actionPerformed(ActionEvent e) {

					try {
				    	  //mendapatkan path file yang ingin di dekrip
				        String a = txtPath2.getText();
				        File pathFolder = new File(a);
				        File currentPath = new File (pathFolder.getParent());
				        String currentFolder = currentPath.toString();
				        a = a.replace("\\", "/"); // replace agar path bisa dibaca java
				       
				        String keyy = txtPath.getText();// path untuk file key
				        keyy = keyy.replace("\\", "/");
				        
				        String fileName = pathFolder.getName();
						
							// dekripsi untuk file selain txt misalnya, mp3,mp4,exe dll
							byte[] imageCipherByte = filetoByteArray(a);
			        		byte[] ivBytes = new byte[] {(byte)0x69,(byte)0xdd,(byte)0xa8,(byte)0x45,(byte)0x5c,(byte)0x7d,(byte)0xd4,(byte)0x25,(byte)0x4b,(byte)0xf3,(byte)0x53,(byte)0xb7,(byte)0x73,(byte)0x30,(byte)0x4e,(byte)0xec};

							String cipherr;
							cipherr = readKeyText(keyy);
							byte[] keyByte = hexStringToByteArray(cipherr);
							//Initialisation
					         SecretKeySpec key = new SecretKeySpec(keyByte, "AES");
					         IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
					         
					         Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5Padding");
					         byte[] enkripsiByte = decrypt(key,ivSpec,imageCipherByte,cipher); // method decrypt
					         OutputStream out = null;

					         try {
					             out = new BufferedOutputStream(new FileOutputStream(currentFolder + "//decrypted-" + fileName));
					             out.write(enkripsiByte);
					         } finally {
					             if (out != null) out.close();
					         }
					         
						
					} catch (IOException e1) {
						
						e1.printStackTrace();
					} catch (NoSuchAlgorithmException e1) {
						e1.printStackTrace();
					} catch (NoSuchPaddingException e1) {
						e1.printStackTrace();
					} catch (InvalidKeyException e1) {
						JOptionPane.showMessageDialog(frame,
							    "Decrypt Failed"); // ERROR handling invalid key
						e1.printStackTrace();
					} catch (InvalidAlgorithmParameterException e1) {
						e1.printStackTrace();
					} catch (IllegalBlockSizeException e1) {
						e1.printStackTrace();
					} catch (BadPaddingException e1) {
						e1.printStackTrace();
					} catch (NullPointerException e2){
						JOptionPane.showMessageDialog(frame,
							    "Field Cannot be Empty"); //EROR unutk field kosong
					}
			        
			      }
			      
			    });
		    
		    btnEncrypt.addActionListener(new ActionListener() {
		    	
			      public void actionPerformed(ActionEvent e) {
			      

			        try {
				    	  // mendapatkan path ke file yang ingin di enkrip
				        String a = txtPath2.getText();
				        File pathFolder = new File(a);
				        File currentPath = new File (pathFolder.getParent());
				        String currentFolder = currentPath.toString();
				        a = a.replace("\\", "/");
				       
				        // mendapatkan 
				        String keyy = txtPath.getText();
				        keyy = keyy.replace("\\", "/");
				        
				        String fileName = pathFolder.getName();
			        		
			        		//untuk enkripsi file selain .txt
			        		byte[] imagePlainByte = filetoByteArray(a); 
			        		
			        		byte[] ivBytes = new byte[] {(byte)0x69,(byte)0xdd,(byte)0xa8,(byte)0x45,(byte)0x5c,(byte)0x7d,(byte)0xd4,(byte)0x25,(byte)0x4b,(byte)0xf3,(byte)0x53,(byte)0xb7,(byte)0x73,(byte)0x30,(byte)0x4e,(byte)0xec};

							String cipherr;
							cipherr = readKeyText(keyy);
							byte[] keyByte = hexStringToByteArray(cipherr);
							//Initialisation
					         SecretKeySpec key = new SecretKeySpec(keyByte, "AES");
					         IvParameterSpec ivSpec = new IvParameterSpec(ivBytes);
					         Cipher cipher = Cipher.getInstance("AES/CTR/PKCS5Padding");
					         byte[] enkripsiByte = encrypt(key,ivSpec,imagePlainByte,cipher);
					         OutputStream out = null;

					         try {
					             out = new BufferedOutputStream(new FileOutputStream(currentFolder  +"//encrypted-" + fileName));
					             out.write(enkripsiByte);
					         } finally {
					             if (out != null) out.close();
					         }
					         
						
					} catch (IOException e1) {
						JOptionPane.showMessageDialog(frame,
							    "Encrypt Failed");
						e1.printStackTrace();
					} catch (NoSuchAlgorithmException e1) {
						e1.printStackTrace();
					} catch (NoSuchPaddingException e1) {
						e1.printStackTrace();
					} catch (InvalidKeyException e1) {
						JOptionPane.showMessageDialog(frame,
							    "Encrypt Failed");
						e1.printStackTrace();
					} catch (InvalidAlgorithmParameterException e1) {
						e1.printStackTrace();
					} catch (IllegalBlockSizeException e1) {
						e1.printStackTrace();
					} catch (BadPaddingException e1) {
						e1.printStackTrace();
					} catch(NullPointerException e2){
						JOptionPane.showMessageDialog(frame,
							    "Field Cannot be Empty");
					}
			        
			      }
			      
			    });
		    
		    
		    //Action listener unutk button browse
		    btnBrowse.addActionListener(new ActionListener() {
		    	
		      public void actionPerformed(ActionEvent e) {
		        JFileChooser fileChooser = new JFileChooser();
		 
		        // For File
		        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		        fileChooser.setAcceptAllFileFilterUsed(false);
		        int rVal = fileChooser.showOpenDialog(null);
		        if (rVal == JFileChooser.APPROVE_OPTION) {
		          txtPath.setText(fileChooser.getSelectedFile().toString()); 
		        }
		       
		      }
		      
		    });
		    
		    btnBrowse1.addActionListener(new ActionListener() {
		    	
			      public void actionPerformed(ActionEvent e) {
			        JFileChooser fileChooser = new JFileChooser();
			 
			        // For File
			        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			 
			        fileChooser.setAcceptAllFileFilterUsed(false);
			 
			        int rVal = fileChooser.showOpenDialog(null);
			        if (rVal == JFileChooser.APPROVE_OPTION) {
			          txtPath2.setText(fileChooser.getSelectedFile().toString());
			        }
			        
			      }
			      
			    });
		    */
		}

}