import java.io.*;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

import javax.crypto.CipherOutputStream;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.io.FileReader;
import java.io.BufferedReader;


public class XTS18 {

	public static final int BLOCK_SIZE = 16;
	public static final String tweakString = "0123456789ABCDEF";
	private static JFrame frame;
	private static JTextField fileLocation;
	private static JTextField keyLocation;
	private static JTextField txtPath;
	private static String filePath;
	private static String keyPath;
	private static String resultPath;
	private static String fileName;

	 /**
	 * Launch the application.
	 */
	public static void main (String[] args)
	{
		EventQueue.invokeLater (new Runnable () {
			public void run ()
			{
				try {
					XTS18 window = new XTS18();
					window.frame.setVisible (true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static void inputFile(int activity) throws IOException {
		Path wiki_path = Paths.get(filePath);
		byte[] file = null;
		try {
			file = Files.readAllBytes(wiki_path);
		} catch (IOException e) {
			System.out.println(e);
		}

		// convert file to array of bytes
		int fileByteCount = 0;
		int j = 0;
		boolean needStealing = true;
		int unusedSpace = 0;
		if(file.length % BLOCK_SIZE == 0) {
			j = file.length/BLOCK_SIZE;
			needStealing = false;
		}
		else {
			j = (file.length/BLOCK_SIZE)+1;
			needStealing = true;
			unusedSpace = BLOCK_SIZE - (file.length % BLOCK_SIZE);
		}

		// Group the message to 2d array
		// column (first dimension) is per block in file
		// row (second dimension) is per byte in one block
		// each block has BLOCK_SIZE bytes
		byte[][] fileBlocks = new byte[j][BLOCK_SIZE];
		for(int i = 0; i < j; i++) {
			for(int k = 0; k < BLOCK_SIZE; k++) {
				if(fileByteCount < file.length) {
					fileBlocks[i][k] = file[fileByteCount];
					fileByteCount++;
				}
			}
		}

		// Read key
		BufferedReader inputKey = new BufferedReader(new FileReader(new File(keyPath)));
		// Key still in HEX
		String keyStr = inputKey.readLine();
		int keyLength = keyStr.length();
		String keyHex1 = keyStr.substring(0, keyLength/2);
		String keyHex2 = keyStr.substring(keyLength/2, keyLength);
		
		// Convert each key to its char/ascii
		int a = 0;
		String key1 = "";
		while(a < keyHex1.length()) {
			String temp = keyHex1.substring(a,a+2);
			int hex = Integer.parseInt(temp, BLOCK_SIZE);
			key1 += (char)hex;
			a = a+2;
		}
		byte[] key1arr = key1.getBytes();
		
		a = 0;
		String key2 = "";
		while(a < keyHex2.length()) {
			String temp = keyHex2.substring(a,a+2);
			int hex = Integer.parseInt(temp, BLOCK_SIZE);
			key2 += (char)hex;
			a = a+2;
		}
		byte[] key2arr = key2.getBytes();
		System.out.println("file length" + file.length + "\n" + "j:" + j + "\n" + "key1: " + key1 + "\n" + "key2: " +  key2);
		
		// Tweak
		byte[] tweakArr = tweakString.getBytes();
		byte[] tweak = new byte[tweakArr.length];

		// Make it little-endian
		for(int idx = 0; idx < tweakArr.length; idx++) {
			tweak[tweakArr.length-(idx+1)] = tweakArr[idx];
		}
		
		XTS xts = new XTS();

		// Encrypt
		byte[][] ciphertext = xts.xts(activity, fileBlocks, j, key1arr, key2arr, tweak, needStealing, unusedSpace);
		// byte[] final_result = new byte[ciphertext.length];
		// for(int i=0; i< ciphertext.length; i++){
		// 	for(int q = 0; q<16; q++){
		// 		final_result[q] = ciphertext[i][q];
		// 	}
		// }

		//printTwo2DByte(fileBlocks, ciphertext);
		System.out.println("========================================");

		// filepath == "haskjf/sdafas/out.png"
		// seharusnya "haskjf/sdafas/out-encrypted.png"
		String appendix = (activity == XTS.ENCRYPT)
			? "-encrypted"
			: "-decrypted";
		String pathOut = filePath.substring(0, filePath.length() - 4) + appendix 
						 + filePath.substring(filePath.length() - 4, filePath.length());
		writeToFile(ciphertext, pathOut, file.length);
		
		// Close files
		inputKey.close();
	}

	public static void writeToFile(byte[][] target, String filename, int messageLength) throws IOException{ 
		  	byte[] flattenedArray = new byte[messageLength];
		  	int globalID = 0;
			for(int xID = 0; xID < target.length; xID++){
				for(int yID = 0; yID <= 15; yID++){
					if(globalID < messageLength){
			 			flattenedArray[globalID] = (target[xID][yID]);
			 			globalID++;
					}
		   		}
		  	}
		  
			// printing to file
			Path filepath = Paths.get(filename);
			Files.write(filepath, flattenedArray);
		}

	/**
	 * Create the application.
	 */
	public XTS18 ()
	{
		initialize();
	}

	 private void initialize() 
	 {
	 		frame = new JFrame();
			frame.setBounds(100, 100, 600, 300);
			// x,y, width,height
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			frame.getContentPane().setBackground(new Color(255,255,255));
			
			//Label GUI
			JLabel label = new JLabel("XTS AES Encryption/Decryption");
			label.setBounds(100, 10, 375, 24);
			label.setFont(new Font("Roboto Bold", Font.PLAIN, 18));
			frame.getContentPane().add(label);
			
			// input File
			JLabel labelFile = new JLabel("File :");
			labelFile.setBounds(10, 50, 40, 24);
			frame.getContentPane().add(labelFile);
			
			fileLocation = new JTextField();
			fileLocation.setBounds(50, 50, 414, 24);
			frame.getContentPane().add(fileLocation);
			fileLocation.setColumns(10);
			
			JButton btnBrowseFile = new JButton("Browse");
			btnBrowseFile.setBounds(475, 50, 87, 23);
			btnBrowseFile.setBackground(new Color(118, 123, 145));
			btnBrowseFile.setForeground(new Color(255, 255, 255));
			frame.getContentPane().add(btnBrowseFile);
		  
		 	// input key file
			JLabel labelKey = new JLabel("Key :");
			labelKey.setBounds(10, 90, 50, 24);
			frame.getContentPane().add(labelKey);
			
			keyLocation = new JTextField();
			keyLocation.setBounds(50, 90, 414, 24);
			frame.getContentPane().add(keyLocation);
			keyLocation.setColumns(10);
				 
			JButton btnBrowseKey = new JButton("Browse");
			btnBrowseKey.setBounds(475, 90, 87, 23);
			btnBrowseKey.setBackground(new Color(118, 123, 145));
			btnBrowseKey.setForeground(new Color(255, 255, 255));
			frame.getContentPane().add(btnBrowseKey);

			//Button encrypt dan decrypt
			JButton btnEncrypt = new JButton("Encrypt");
			btnEncrypt.setBounds(50, 130, 87, 23);
			btnEncrypt.setBackground(new Color(118, 123, 145));
			btnEncrypt.setForeground(new Color(255, 255, 255));
			frame.getContentPane().add(btnEncrypt);
			
			JButton btnDecrypt = new JButton("Decrypt");
			btnDecrypt.setBounds(150, 130, 87, 23);
			btnDecrypt.setBackground(new Color(118, 123, 145));
			btnDecrypt.setForeground(new Color(255, 255, 255));
			frame.getContentPane().add(btnDecrypt);
			
			// Footer
			JLabel footer = new JLabel("Created with love by Sang Agung, Putri Ramadhanty & Andhita Nurul Ainun");
			footer.setBounds(75, 220, 450, 40);
			footer.setFont(new Font("Roboto Bold", Font.PLAIN, 13));
			btnDecrypt.setForeground(new Color(29, 45, 68));
			frame.getContentPane().add(footer);


			//Action listener unutk button browse File
			btnBrowseFile.addActionListener(new ActionListener() {
				
			  public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
		 
				// For File
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fileChooser.setAcceptAllFileFilterUsed(false);
				int rVal = fileChooser.showOpenDialog(null);
				if (rVal == JFileChooser.APPROVE_OPTION) {
				  fileLocation.setText(fileChooser.getSelectedFile().toString()); 
				  filePath = fileLocation.getText();
				}
			   
			  }
			  
			});
			
			//Action listener unutk button browse Key
			btnBrowseKey.addActionListener(new ActionListener() {
				
			  public void actionPerformed(ActionEvent e) {
				JFileChooser fileChooser = new JFileChooser();
		 
				// For File
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fileChooser.setAcceptAllFileFilterUsed(false);
				int rVal = fileChooser.showOpenDialog(null);
				if (rVal == JFileChooser.APPROVE_OPTION) {
				  keyLocation.setText(fileChooser.getSelectedFile().toString());
				  keyPath = keyLocation.getText();
				}
				
			  }
				  
			 });

			//Button menjalankan fungsi encrypt
			btnEncrypt.addActionListener (new ActionListener () {
				@Override
				public void actionPerformed (ActionEvent arg0)
				{
					// Pencegahan error apabila tidak ada file atau key yang dimasukkan
					// Memunculkan alert warning
					if (filePath == null || keyPath == null) {
						JOptionPane.showMessageDialog (null,
								"Cannot be Empty", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					} else {
						// Melakukan enkripsi dengan cara memanggil method encryption pada kelas Encrypt
						try{
							inputFile(XTS.ENCRYPT);	
						}
						catch(IOException e){
							System.out.println("gagal");
						}
						

						// Mengubah nama dari hasil file yang dienkripsi
						// Secara default nama file bagian depan berisikan "cipher_of_"
						setResultPath (resultPath + "encrypt_result_of_" + fileName);
			  						
						// Memunculkan alert yang menandakan enkripsi telah berhasil
						JOptionPane.showMessageDialog (null,
							"File is successfully encrypted.\n Go check " + resultPath, "SUCCESS", 
							JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});

		   btnDecrypt.addActionListener (new ActionListener() {
				@Override
				public void actionPerformed (ActionEvent arg0)
				{
					// Pencegahan error apabila tidak ada file atau key yang dimasukkan
					// Memunculkan alert warning
					if (filePath == null || keyPath == null) {
						JOptionPane.showMessageDialog (null,
								"Cannot be Empty", "WARNING",
								JOptionPane.WARNING_MESSAGE);
					} else {
						// Melakukan dekripsi dengan cara memanggil method decryption pada kelas Encrypt
						try{
							inputFile(XTS.DECRYPT);	
						}
						catch(IOException e){
							System.out.println("gagal");
						}
						

						// Mengubah nama dari hasil file yang dienkripsi
						// Secara default nama file bagian depan berisikan "cipher_of_"
						setResultPath (resultPath + "decrypt_result_of_" + fileName);
			  						
						// Memunculkan alert yang menandakan enkripsi telah berhasil
						JOptionPane.showMessageDialog (null,
							"File is successfully decrypted.\n Go check " + resultPath, "SUCCESS", 
							JOptionPane.INFORMATION_MESSAGE);
					}
				}
			});



	 }

	public void setFilePath (String filePath)
	{
		this.filePath = filePath;
	}

	public String getFilePath()
	{
		return filePath;
	}


	public void setKeyPath (String keyPath)
	{
		this.keyPath = keyPath;
	}

	 public String getKeyPath()
	{
		return keyPath;
	}

	public void setResultPath (String resultPath)
	{
		this.resultPath = resultPath;
	}

	 public String getResultPath()
	{
		return resultPath;
	}

	 public JTextField getKeyLocation()
	{
		return keyLocation;
	}

	 public JTextField getFileLocation()
	{
		return fileLocation;
	}
}